                                         ###############       MODULE - 6   ###############

                 ###################################  FUNCTION FOR CAPACITY CHECK #################################

#IF THE CAPACITY IS GREATER THAN THE MAX.CAPACITY AND IF THE PLAN VALUES HAS TO BE MOVED TO THE PREVIOUS WEEKS
# 1) Smallest cover value is selected 
# 2) Its corresponding group values are reterived
# 3) Plan values of this group is added to the previous week , If the previous week's capacity doesnt exceed its max capacity. 
                                         
#IF THE CAPACITY IS GREATER THAN THE MAX. CAPACITY AND IF THE PLAN VALUES HAS TO BE MOVED TO THE NEXT WEEKS.
# 1) Largest cover value is selected 
# 2) Its corresponding group values are reterived.
# 3) if the cover values of all the skus in that group is meeting the target cover, 
#    Plan values of this group is added to the next week. 
                                         
#capacity of each week is recomputed.
#If it is still not meeting the capacity constraint,
# 1) Largest cover value is selected 
# 2) Its corresponding group values are reterived.
# 3) Cover values of all the skus in that group is greater than 2.5 , 
#    Plan values of this group is added to the next week. 
# Even after this step , if the capacity contraint is not met, then the cover values are compared if they are greater than
# 1.5 or 0.5 and then plan values are added to the next week accordingly. 
                                         
source_data<-`minquant 4` 

capacity_check<-function(source_data)
{
library(zoo)

# selecting required data for merging(SKU,MOQ,Group,SKU_type,Interchange_sku)
firstfew<-week13[,c(1:3,5:6)]
source_data<-merge(firstfew,source_data[,c(1,4:ncol(source_data))],by="SKU")



merge_1<-source_data


all_make_to_order<-data.frame()
required_MTOdata_to_include_inweek2<-data.frame()
unique_groups<-unique(merge_1$Group)
plancheck<-grep("plancheck",colnames(merge_1))

#computing capacity of each week 
column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
print(column_sums)

#retrieving dates in current month
index_1<-grep("INTERCHANGE_SKU_NUMBER",colnames(merge_1))+1
index_2<-grep("plan_sum",colnames(merge_1))-1
date.seq<-colnames(merge_1)[index_1:index_2]
date.seq<-substr(date.seq,1,10)

#computing no., of weeks in each month
#Finding which week is present in the current input month
#required_week_info<-year_week_info()


# if(required_week_info[nrow(required_week_info),2]== "01") 
#   {
#   required_week_info<-required_week_info[-nrow(required_week_info),]
#   }
# 
# val_eod <- aggregate(required_week_info, by = list(required_week_info$Months), FUN = tail, n = 1)
# tail_merge<-merge(required_week_info,val_eod[,c(3,4)],by="Months")
# tail_merge$capacity<-ifelse(tail_merge$frequency.x == tail_merge$frequency.y,150,825)
# capacity<-tail_merge[,c(1,2,3,5)]

capacity_line_info<-read.csv("C:\\Users\\NidhiS\\Desktop\\Testing files\\Capacity and Line Information.csv")
capacity_line_info$Capacity_new<-(capacity_line_info$Capacity-15)
capacity<-capacity_line_info
capacity$Date<-as.Date(capacity$Date,"%d-%m-%Y")
capacity$Date<-as.character(capacity$Date)
Final_cap<-capacity[match(date.seq,capacity$Date),ncol(capacity)]

week_check<-required_week_info[match(date.seq,required_week_info$Dates),ncol(required_week_info)]
week_check<-unlist(week_check[,1])


#######################################################################################################
# Priority for MTOs are given in 2nd week.
# if the input month has 1st and 2nd week ,
# Pure MTO groups containing plan values are added to 2nd week.
if(any(week_check %in% 1) & any(week_check %in% 2))
{
if(column_sums[1] > Final_cap[1] & column_sums[2]< Final_cap[2])
{
  for(k in 1:length(unique_groups))
  {
    print(k)
    subset_allMTO<-merge_1[merge_1$Group == unique_groups[k],]
    logical<-all(subset_allMTO$SKU_TYPE == "make to order",na.rm = T)
    if(logical == TRUE)
    {
      print("week2 plan is not updated but stored for cross checking") 
      all_make_to_order<-rbind(all_make_to_order,subset_allMTO)
      
    }
    
    if(logical == TRUE  &  subset_allMTO$groupsumall13wks[1] > 0 )
    {
      print("week2 plan is being updated , all possible MTO skus are shifted to week2")
      required_MTOdata_to_include_inweek2<-subset_allMTO
      required_MTOdata_to_include_inweek2[,plancheck[2]]<-required_MTOdata_to_include_inweek2$plan_sum
      plancheck_new<-plancheck[-2]
      required_MTOdata_to_include_inweek2[,plancheck_new]<-0
      required_MTOdata_to_include_inweek2<-close_cover_update(required_MTOdata_to_include_inweek2)
      group_num <- required_MTOdata_to_include_inweek2$Group
      for(k in 1:length(group_num))
      {
      merge_1<-merge_1[!merge_1$Group == group_num[k],]
      }
      merge_1<-rbind(merge_1,required_MTOdata_to_include_inweek2)
      
    }
    
  }
  
}
}

###########################################################################################################

# computing capacity after moving MTOs to 2nd week. 
column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
print(column_sums)
column_sums_1<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
cover.out<-grep("cover.out",colnames(merge_1))
plancheck<-grep("plancheck",colnames(merge_1))
cover.in<-grep("cover.In",colnames(merge_1))



#checking capacity of the weeks and moving the plans accordingly.
for(m in 1:length(plancheck)) 
{
  merge_1_copy<-merge_1
  merge_2<-merge_1
  k=1
  column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
  cover.out<-grep("cover.out",colnames(merge_1))
  
  ###################################################################################################################
  #if the capacity is greater than the max. capacity,if the plan values has to be moved to the previous weeks
  # 1) Smallest cover value is selected 
  # 2) Its corresponding group values are reterived
  # 3) Plan values of this group is added to the previous week , If the previous week's capacity doesnt exceed its max capacity. 
  
  if(m>1)
  {
    if(column_sums[m] > Final_cap[m] & column_sums[m-1]< Final_cap[m-1])
    {
      #Cover values apart from Nas and Inf are reterived.
      column1b <- merge_1[,cover.out[m]][which(!is.na(merge_1[,cover.out[m]]))]
      column1c <- round(column1b[which(column1b < Inf)],6)
      
      
      
      for(k in 1:length(column1c))
      {
        print("loop entered")
        cat(k,"th lowest")
         
        min_numb<-sort(column1c, FALSE)[k]
        group_num<-merge_1[grep(min_numb,round(merge_1[,cover.out[m]],6)),3]
        
        for(j in 1:length(group_num))
          
        {
          print(group_num[j])
          subset_group.num<-merge_1[merge_1$Group == group_num[j],]
          print("switch plans to 2nd week")
          
          subset_group.num[,plancheck[m-1]]<-subset_group.num[,plancheck[m-1]]+subset_group.num[,plancheck[m]]
          subset_group.num[,plancheck[m]]<-0
          
          subset_group.num<-close_cover_update(subset_group.num)
          merge_2<-merge_2[!merge_2$Group == group_num[j],]
          merge_2<-rbind(merge_2,subset_group.num)
          column_sums_5<-colSums(merge_2[,(grep("plancheck",colnames(merge_2)))])
          if(column_sums_5[m-1]>Final_cap[m-1])
          {
            break
          }
          
          merge_1<-merge_1[!merge_1$Group == group_num[j],]
          merge_1<-rbind(merge_1,subset_group.num)
          merge_1<-close_cover_update(merge_1)
          column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
          if(column_sums[m] <= Final_cap[m])
          {
            print("capacity met_1")
            break
          }
        }
        
        column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
        print(column_sums)
        
        if(column_sums[m] <= Final_cap[m])
        {
          print("capacity met")
          break
        }
      }
    }#if
    
  }
  cat("actual sum of plans",column_sums_1,"\n")
  cat("updated sum of plans",column_sums,"\n")
  
  
  ###################################################################################################################
  #if the capacity is greater than the max. capacity and if the plan values has to be moved to the next weeks.
  # 1) Largest cover value is selected 
  # 2) Its corresponding group values are reterived.
  # 3) if the cover values of all the skus in that group is meeting the target cover, 
  #    Plan values of this group is added to the next week. 

  
  
  
  if(column_sums[m] > Final_cap[m] & m != length(column_sums))
  {
    merge_1_copy[,plancheck[m]]<-0
    merge_1_copy<-close_cover_update(merge_1_copy)
    column1b <- merge_1_copy[,cover.out[m]][which(!is.na(merge_1_copy[,cover.out[m]]))]
    column1c <- round(column1b[which(column1b < Inf)],6)
    
    for(k in m:length(column1c))
    {
      print("loop entered")
      cat(k,"th highest")
      max_numb<-sort(column1c, TRUE)[k]
      group_num<-merge_1_copy[grep(max_numb,round(merge_1_copy[,cover.out[m]],6)),3]
      
      for(j in 1:length(group_num))
      {
        subset_group.num<-merge_1[merge_1$Group == group_num[j],]
        subset_group.num_copy<-merge_1_copy[merge_1_copy$Group == group_num[j],]
        if(all(round(subset_group.num_copy[,cover.out[m]],2) >= round(subset_group.num_copy[,cover.in[m]],2),na.rm = T))
        {
          
          print("switch plans to 2nd week")
          subset_group.num[,plancheck[m+1]]<-subset_group.num[,plancheck[m]]+subset_group.num[,plancheck[m+1]]
          subset_group.num[,plancheck[m]]<-0
          subset_group.num<-close_cover_update(subset_group.num)
          merge_1<-merge_1[!merge_1$Group == group_num[j],]
          merge_1<-rbind(merge_1,subset_group.num)
          merge_1<-close_cover_update(merge_1)
          
        }
      }
      
      
      column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
      print(column_sums)
      if(column_sums[m] <= Final_cap[m])
      {
        print("capacity met")
        break
      }
    }
  }#if
  cat("actual sum of plans",column_sums_1,"\n")
  cat("updated sum of plans",column_sums,"\n")
  
  #################################################################################################################
  #capacity of each week is recomputed.
  #If it is still not meeting the capacity constraint,
  # 1) Largest cover value is selected 
  # 2) Its corresponding group values are reterived.
  # 3) Cover values of all the skus in that group is greater than 2.5 , 
  #    Plan values of this group is added to the next week. 
  # Even after this step , if the capacity contraint is not met, then the cover values are compared if they are greater than
  # 1.5 or 0.5 and then plan values are added to the next week accordingly. 
  
  values<-c(2.5,1.5,1,0.5)
  
  for( n in 1:length(values))
  {
  print(values[n])
  if(column_sums[m] > Final_cap[m] & m != length(column_sums))
  {
    merge_1_copy[,plancheck[m]]<-0
    merge_1_copy<-close_cover_update(merge_1_copy)
    column1b <- merge_1_copy[,cover.out[m]][which(!is.na(merge_1_copy[,cover.out[m]]))]
    column1c <- column1b[which(column1b < Inf)]
    
    for(k in 1:length(column1c))
    {
      print("loop entered")
      
      cat(k,"th highest")
      max_numb<-sort(column1c, TRUE)[k]
      max_row_num<-grep(max_numb,merge_1_copy[,cover.out[m]])
      group_num<-merge_1_copy[max_row_num,3]
      
      for(j in 1:length(group_num))
      {
        subset_group.num<-merge_1[merge_1$Group == group_num[j],]
        subset_group.num_copy<-merge_1_copy[merge_1_copy$Group == group_num[j],]
        if(all(round(subset_group.num_copy[,cover.out[m]],2) >= values[n],na.rm = T))
        {
          subset_group.num[,plancheck[m+1]]<-subset_group.num[,plancheck[m]]+subset_group.num[,plancheck[m+1]]
          subset_group.num[,plancheck[m]]<-0
          subset_group.num<-close_cover_update(subset_group.num)
          merge_1<-merge_1[!merge_1$Group == group_num[j],]
          merge_1<-rbind(merge_1,subset_group.num)
          merge_1<-close_cover_update(merge_1)
          
        }
      }
      column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
      print(column_sums)
      if(column_sums[m] <= Final_cap[m])
      {
        print("capacity met")
        break
      }
    }
  }#if
  cat("actual sum of plans",column_sums_1,"\n")
  cat("updated sum of plans",column_sums)
  }# for( cover values comparision)  
  
}#for
#################################################################################################

if(any(column_sums > Final_cap))
{
for(m in 1:length(plancheck))
{
  updated_group_index<-c()
  if(m>1){
  unique_groups<-sort(unique_groups)
  if(column_sums[m] > Final_cap[m] )
  {
    for(k in 1:length(unique_groups))
    {
      merge_1_copy<-merge_1
      print(unique_groups[k])
      subset_allMTO<-merge_1[merge_1$Group == unique_groups[k],]
      logical<-all(subset_allMTO$SKU_TYPE == "make to order",na.rm = T)
      if(logical == TRUE  &  subset_allMTO$groupsumall13wks[1] > 0 )
      {
        print("week2 plan is being updated , all possible MTO skus are shifted to week2")
        required_MTOdata_to_include_inweek2<-subset_allMTO
        required_MTOdata_to_include_inweek2[,plancheck[m-1]]<-required_MTOdata_to_include_inweek2$plan_sum
        plancheck_new<-plancheck[-(m-1)]
        required_MTOdata_to_include_inweek2[,plancheck_new]<-0
        required_MTOdata_to_include_inweek2<-close_cover_update(required_MTOdata_to_include_inweek2)
        group_num <- required_MTOdata_to_include_inweek2$Group
        for(k in 1:length(group_num))
        {
          merge_1_copy<-merge_1_copy[!merge_1_copy$Group == group_num[k],]
        }
        merge_1_copy<-rbind(merge_1_copy,required_MTOdata_to_include_inweek2)
        column_sums_5<-colSums(merge_1_copy[,(grep("plancheck",colnames(merge_1_copy)))])
        if(column_sums_5[m-1]>=Final_cap[m-1])
        {
          break
        }
        for(k in 1:length(group_num))
        {
          updated_group_index<-group_num
          merge_1<-merge_1[!merge_1$Group == group_num[k],]
        }
        merge_1<-rbind(merge_1,required_MTOdata_to_include_inweek2)
        column_sums_6<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
        print(column_sums_6)
      }
      
    }
    
  }
  column_sums_6<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
  print(column_sums_6)
}
  column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
  
if(length(updated_group_index) >= 1){
  updated_group_index<-unique(updated_group_index)
  unique_groups<-unique_groups[-which(unique_groups == updated_group_index)]}
  unique_groups<-sort(unique_groups)
  if(column_sums[m] > Final_cap[m] & (m != length(plancheck)-1 | m != length(plancheck) ) )
  {
    for(k in 1:length(unique_groups))
    {
      #merge_1_copy<-merge_1
      print(unique_groups[k])
      subset_allMTO<-merge_1[merge_1$Group == unique_groups[k],]
      logical<-all(subset_allMTO$SKU_TYPE == "make to order",na.rm = T)
      if(logical == TRUE  &  subset_allMTO$groupsumall13wks[1] > 0 )
      {
        print("week2 plan is being updated , all possible MTO skus are shifted to week2")
        required_MTOdata_to_include_inweek2<-subset_allMTO
        required_MTOdata_to_include_inweek2[,plancheck[m+1]]<-required_MTOdata_to_include_inweek2$plan_sum
        plancheck_new<-plancheck[-(m+1)]
        required_MTOdata_to_include_inweek2[,plancheck_new]<-0
        required_MTOdata_to_include_inweek2<-close_cover_update(required_MTOdata_to_include_inweek2)
        group_num <- required_MTOdata_to_include_inweek2$Group
        
        for(k in 1:length(group_num))
        {
          
          merge_1<-merge_1[!merge_1$Group == group_num[k],]
        }
        
        merge_1<-rbind(merge_1,required_MTOdata_to_include_inweek2)
        
        column_sums_5<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
        print(column_sums_5)
        
        if(column_sums_5[m] <= Final_cap[m])
        {
          print(unique_groups[k])
          print("loop broke")
          break
        }
        
      }
      
    }
    
  }
  column_sums<-colSums(merge_1[,(grep("plancheck",colnames(merge_1)))])
}
}
plancol<-grep("plancheck",colnames(merge_1))
for(m in length(plancol):2)
{
  merge_1_copy<-merge_1
  merge_1_copy[,plancol[m-1]]<-merge_1_copy[,plancol[m-1]]+merge_1_copy[,plancol[m]]
  merge_1_copy[,plancol[m]]<-0
  column_sums<-colSums(merge_1_copy[,(grep("plancheck",colnames(merge_1_copy)))])
  if(column_sums[m-1] > Final_cap[m-1])next
  merge_1[,plancol[m-1]]<-merge_1[,plancol[m-1]]+merge_1[,plancol[m]]
  merge_1[,plancol[m]]<-0
}
merge_1<-close_cover_update(merge_1)
return(merge_1)
}

