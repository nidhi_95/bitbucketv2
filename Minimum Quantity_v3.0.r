                                                  ##### MODULE 5 #####

                                            #### MINIMUM QUANTITY UPDATE ####


minquantity_function<-function(monthly_dataframe)
{
  numcol.a<-length(grep("plancheck",colnames(monthly_dataframe)))
  #print("select min quantity file")
  #min_quantity<-read.csv("Min_quantity.csv")
  plan_index<-grep("plancheck",colnames(monthly_dataframe))
  monthly_dataframe<-merge(monthly_dataframe,min_quantity,by="SKU",all.x = T)
  count<-0
  repeat
  {
    count<-count+1
    for(m in 1:nrow(monthly_dataframe))
    {
      #if all the values in a row are Nas, then that particular iteration in skipped. 
      if(all(is.na(monthly_dataframe[m,plan_index])))next
###########################################################################################################################      
                                  ########################      STEPS    ################################
      
      ##if plan value of an sku in any week is lesser than the minimum quantity :
      # 1) If plan value is lesser than the minimum quantity in only 1week . it is replaced with the min_quantity value.
      # 2) If plan value is lesser than the minimum quantity in more than 1week.
      #    Case 1: if sum of lesser plan values is lesser than the minimum quantity, 1st occurance of the lesser plan value is replaced with the minimum quantity
      #    Case 2: if sum of lesser plan values is greater than or equal to the minimum quantity,1st occurance of the lesser plan value is replaced with the sum of lesser plan values.
      # Close and cover values are updated accordingly.
      
########################################################################################################################     
      
      if(any(monthly_dataframe[m,plan_index] < monthly_dataframe[m,ncol(monthly_dataframe)] & monthly_dataframe[m,plan_index]!=0,na.rm = T))
      {
        values<-monthly_dataframe[m,plan_index]
        index<-plan_index[c(which(monthly_dataframe[m,plan_index] < monthly_dataframe[m,ncol(monthly_dataframe)] & monthly_dataframe[m,plan_index]!=0))]
        values<-monthly_dataframe[m,plan_index[c(which(monthly_dataframe[m,plan_index] < monthly_dataframe[m,ncol(monthly_dataframe)] & monthly_dataframe[m,plan_index]!=0))]]
        #If plan value is lesser than the minimum quantity in more than 1week.
        if(length(values) > 1)
        { 
          
        #if sum of lesser plan values is lesser than the minimum quantity
          if(sum(values) < monthly_dataframe[m,ncol(monthly_dataframe)])
          {
            
        #1st occurance of the lesser plan value is replaced with the minimum quantity
        values[1]<-monthly_dataframe[m,ncol(monthly_dataframe)]
        values[2:ncol(values)]<-0
        index<-match(colnames(values),colnames(monthly_dataframe))
        monthly_dataframe[m,c(index)]<-values
        break
          }
          #if sum of lesser plan values is greater than or equal to the minimum quantity
          if(sum(values)>=monthly_dataframe[m,ncol(monthly_dataframe)])
          {
            #1st occurance of the lesser plan value is replaced with the sum of lesser plan values
            values[1]<-sum(monthly_dataframe[m,plan_index])
            values[2:ncol(values)]<-0
            index<-match(colnames(values),colnames(monthly_dataframe))
            monthly_dataframe[m,c(index)]<-values
            break
          }
          
        }
        else
        {
          #If plan value is lesser than the minimum quantity in only 1week . it is replaced with the min_quantity value.
          monthly_dataframe[m,plan_index[which(values == monthly_dataframe[m,plan_index])]]<-monthly_dataframe[m,ncol(monthly_dataframe)]
          
        }
      
      }
      
    }
    if(count == nrow(monthly_dataframe))
    {
      break
    }
  }
        

  
  for(l in 1:numcol.a)
  {
    #computing close.out
    name_close<-paste0("close.out",l)
    fcst<-grep("fcst",colnames(monthly_dataframe))
    plan<-grep("plancheck",colnames(monthly_dataframe))
    close.out<-grep("close.out",colnames(monthly_dataframe))
    #close = prev.close-fcst+plan
    if(l==1)
    {
      monthly_dataframe[name_close]<-round(monthly_dataframe$pre_Close-monthly_dataframe[,fcst[l]]+monthly_dataframe[,plan[l]],6)
    }
    if(l > 1)
    {
      monthly_dataframe[name_close]<-round(monthly_dataframe[,close.out[l-1]]-monthly_dataframe[,fcst[l]]+monthly_dataframe[,plan[l]],6) 
    }
  }
  
  
  
  # computing cover
  for(m in 1:numcol.a)
  {
    name_cover.out<-paste0("cover.out",m)
    close.out<-grep("close.out",colnames(monthly_dataframe))
    fourwksf<-grep("4wksf",colnames(monthly_dataframe))
    monthly_dataframe[name_cover.out]<-(monthly_dataframe[,close.out[m]]/monthly_dataframe[,fourwksf[m]])*4
  }
  monthly_dataframe<-monthly_dataframe[,-ncol(monthly_dataframe)]
  return(monthly_dataframe)
}
